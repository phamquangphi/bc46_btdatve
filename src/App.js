import logo from "./logo.svg";
import "./App.css";
import Interface from "./BookMovieTickets/Interface";

function App() {
  return (
    <div>
      <Interface />
    </div>
  );
}

export default App;
