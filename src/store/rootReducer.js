import { combineReducers } from "redux";
import { baitapDatVeReducer } from "./BTDatVeTool/slice";

export const rootReducer = combineReducers({
  btDatVeToolKit: baitapDatVeReducer,
});
