import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  chairBooking: [],
  chairPay: [],
};
const BTDatVeSlice = createSlice({
  name: "BTDatVeTool",
  initialState,
  reducers: {
    setChairBooking: (state, actions) => {
      //Check if any seats already exist
      const index = state.chairBooking.findIndex(
        (e) => e.soGhe === actions.payload.soGhe
      );
      //Satisfying conditions for seat selection
      if (index !== -1) {
        //Exist => flase
        state.chairBooking.splice(index, 1);
      } else {
        //not existed yet => true
        state.chairBooking.push(actions.payload);
      }
    },
    setChairPay: (state, { payload }) => {
      state.chairPay = [...state.chairPay, ...state.chairBooking];
      state.chairBooking = [];
    },
  },
});
export const { actions: bookSeatsActions, reducer: baitapDatVeReducer } =
  BTDatVeSlice;
