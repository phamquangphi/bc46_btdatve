import React from "react";
import ListChair from "./ListChair";
import ResultBook from "./ResultBook";
import data from "./data.json";
import "./style.scss";
const Interface = () => {
  return (
    <div className="Interface">
      <h1 className="text-center">Đặt Vé Xem Phim</h1>
      <div className="container mt-3">
        <div className="row">
          <div className="col-8">
            <div className="text-center font-wieght-bold bg-dark text-white mt-3 p-3">
              screen
            </div>
            {/**List chair */}
            <div>
              <ListChair data={data} />
            </div>
          </div>
          {/**Result boooking */}
          <div className="col-4">
            <ResultBook />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Interface;
