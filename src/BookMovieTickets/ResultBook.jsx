import React from "react";
import "./style.scss";
import { useDispatch, useSelector } from "react-redux";
import { bookSeatsActions } from "../store/BTDatVeTool/slice";
const ResultBook = () => {
  const dispatch = useDispatch();
  const { chairBooking } = useSelector((state) => state.btDatVeToolKit);
  return (
    <div className="ResultBook">
      <h3>Danh Sách Ghế đã chọn</h3>
      <div className="d-flex flex-colum mt-3">
        <div className="BGRed">R</div>
        <p className="ml-3 mt-1">Ghế Đang Chọn</p>
      </div>
      <div className="d-flex flex-colum mt-2">
        <div className="BGXam">X</div>
        <p className="ml-3 mt-1">Ghế Đã Được Đặt</p>
      </div>
      <div className="d-flex flex-colum mt-2">
        <div className="BGWhite">W</div>
        <p className="ml-3 mt-1">Ghế Chưa Đặt</p>
      </div>
      <table className="table">
        <thead>
          <tr>
            <td>Số Ghế</td>
            <td>Giá</td>
            <td>Thao Tác</td>
          </tr>
        </thead>
        <tbody>
          {chairBooking.map((chair) => (
            <tr key={chair.soGhe}>
              <td>{chair.soGhe}</td>
              <td>{chair.gia}</td>
              <td>
                <button
                  className="btn Cancel"
                  onClick={() => {
                    dispatch(bookSeatsActions.setChairBooking(chair));
                  }}
                >
                  <i class="fa-solid fa-xmark"></i>
                </button>
              </td>
            </tr>
          ))}
          <tr>
            <td>Tổng Tiền</td>
            <td>
              {chairBooking.reduce((total, ghe) => (total += ghe.gia), 0)}
            </td>
          </tr>
        </tbody>
      </table>
      <button
        className="btn btn-success"
        onClick={() => {
          dispatch(bookSeatsActions.setChairPay());
        }}
      >
        Thanh Toán
      </button>
    </div>
  );
};

export default ResultBook;
