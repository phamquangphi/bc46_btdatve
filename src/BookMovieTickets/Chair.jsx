import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { bookSeatsActions } from "../store/BTDatVeTool/slice";
import cn from "classnames";
import "./style.scss";
const Chair = ({ chair }) => {
  const dispatch = useDispatch();
  const { chairBooking, chairPay } = useSelector(
    (state) => state.btDatVeToolKit
  );
  //   console.log(chairBooking);
  return (
    <div>
      <button
        className={cn("btn btn-outline-dark Chair", {
          booking: chairBooking.find((e) => e.soGhe === chair.soGhe),
          booked: chairPay.find((e) => e.soGhe === chair.soGhe),
        })}
        style={{ width: "50px" }}
        onClick={() => {
          dispatch(bookSeatsActions.setChairBooking(chair));
        }}
      >
        {chair.soGhe}
      </button>
    </div>
  );
};

export default Chair;
