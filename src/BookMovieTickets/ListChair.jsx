import React from "react";
import Chair from "./Chair";

const ListChair = ({ data }) => {
  return (
    <div>
      {data.map((listChair) => {
        return (
          <div className="d-flex mt-3" style={{ gap: "10px" }}>
            <div className="text-center" style={{ width: "40px" }}>
              {listChair.hang}
            </div>
            <div className="d-flex" style={{ gap: "10px" }}>
              {listChair.danhSachGhe.map((chair) => {
                return <Chair chair={chair} key={chair.soGhe} />;
              })}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default ListChair;
